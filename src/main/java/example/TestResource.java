package example;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("resource")
class TestResource {

    private final TestService service;

    TestResource(TestService service) {
        this.service = service;
    }

    @GetMapping("greet/{name}")
    String greet(@PathVariable String name) {
        return "Hello, " + name + "!";
    }

    @GetMapping("sum")
    int sum(@RequestParam("a") int a, @RequestParam("b") int b) {
        return service.add(a, b);
    }

}
