package example;

import org.springframework.stereotype.Service;

@Service
class TestService {

    int add(int a, int b) {
        return a + b;
    }

}
