package example

import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import spock.lang.Specification

/**
 * @WebMvcTest can be used if rest template bean is provided be user.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class IntegrationResourceTest extends Specification {

    @Autowired
    private TestRestTemplate restTemplate

    def "greet"() {
        expect:
        "Hello, Julian!" == restTemplate.getForObject("/resource/greet/Julian", String)
    }

}
