package example

import spock.lang.Specification

class UnitResourceTest extends Specification {

    private TestService service = Mock(TestService)

    private TestResource testResource = new TestResource(service)

    def "sum"() {
        given:
        service.add(_, _) >> 20

        expect:
        testResource.sum(8, 9) == 20
    }

}
